<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 10:17 AM
 */

namespace Jtangas\AuthBundle\Exception;


use Exception;
use Jtangas\UtilityBundle\Utility;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class JTException extends Exception
{
    const EXCEPTION_CODES = [
        AccessDeniedException::class                => 401,
        AuthenticationException::class              => 401,
    ];

    protected $messages;
    protected $details = '';
    protected $topMessageOverride;
    protected $jtErrorCode = null;
    /**
     * @var Exception $innerException
     */
    protected $innerException;
    protected $httpCode;
    protected $context = [];

    /**
     * JTException constructor.
     * @param string|string[] $messages
     * @param null $httpCode
     * @param Exception|null $innerException
     * @param array|null $context
     * @param null $setTopMessage
     * @throws Exception
     */
    public function __construct(
        $messages,
        $httpCode = null,
        Exception $innerException = null,
        array $context = null,
        $setTopMessage = null
    ) {
        switch (gettype($messages)) {
            case 'array':
                foreach($messages as $messageText) {
                    $this->addMessage($messageText);
                }
                break;
            case 'string':
                $this->addMessage($messages);
                if ($setTopMessage == true) {
                    $this->topMessageOverride = $messages;
                }
                break;
            default:
                $m = 'JTException::__construct $messages must be a string or an array of strings';
                throw new \Exception($m);
                break;
        }

        $this->setHttpCode($httpCode);
        if (!is_null($innerException)) {
            $this->setInnerException($innerException);
        }

        $this->addContext($context);
    }

    public function addMessage($messageText)
    {
        if (!is_string($messageText)) {
            $m = 'JTException::addMessage $messageText must be a string';
            throw new \Exception($m);
        }

        $this->messages[] = $messageText;
    }

    public function setHttpCode($httpCode)
    {
        if (is_null($httpCode)) {
            return;
        }

        if (!is_integer($httpCode)) {
            $m = 'JTException::addHttpCode $httpCode must be an integer';
            throw new \Exception($m);
        }

        $this->httpCode = $httpCode;
    }

    public function setInnerException(Exception $exception)
    {
        if (is_null($exception)) {
            return;
        }

        $this->innerException = $exception;

        if (is_null($this->httpCode)) {
            $class = get_class($exception);

            $this->setHttpCode(Utility::get(self::EXCEPTION_CODES[$class], 400));

            switch (true) {
                case $this->innerException instanceof AuthenticationException:
                    $this->parseAuthenticationException();
                    break;
                default:
                    break;
            }
        }
    }

    protected function parseAuthenticationException()
    {
        $isInvalidJwt = preg_match('/Invalid JWT Token/', $this->innerException->getMessage());
        if ($isInvalidJwt) {
            $this->httpCode = 452;
        }
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function getHttpCode()
    {
        return $this->httpCode;
    }

    public function getInnerException()
    {
        return $this->innerException;
    }

    public function getTopMessageOverride()
    {
        return $this->topMessageOverride;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function addContext($newContext)
    {
        if (is_array($newContext)) {
            $this->context = array_merge($this->context, $newContext);
        } else {
            $this->context[] = $newContext;
        }
    }

    public function getJtErrorCode()
    {
        return $this->jtErrorCode;
    }
}