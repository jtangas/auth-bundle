<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 10:12 AM
 */

namespace Jtangas\AuthBundle\EventListener;


use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class JwtSuccessListener
{
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();

        $data['success'] = true;
        $data['message'] = 'Login Successful';
        $data['data'] = ['token' => $data['token']];
        unset($data['token']);

        $event->setData($data);
    }
}