<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 10:11 AM
 */

namespace Jtangas\AuthBundle\EventListener;


use Jtangas\AuthBundle\Exception\JTException;
use Jtangas\AuthBundle\Traits\JsonApiTrait;
use Jtangas\UtilityBundle\Utility;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\VarDumper\VarDumper;

class ExceptionListener
{
    use JsonApiTrait;

    const ERROR_CODE_INVALID_FIELD = 1001;
    const ERROR_CODE_INVALID_TOKEN = 1002;
    const ROUTE_ERRORS = [
    ];

    protected $extraDebug = true;

    function __construct($extraDebug)
    {
        $this->extraDebug = $extraDebug;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        if ($exception) {
            if (!($exception instanceof JTException)) {
                $exception = new JTException(
                    'Unhandled Exception at ' . $exception->getTraceAsString(),
                    null,
                    $exception
                );
            }

            $messages   = $exception->getMessages();
            $tracer     = $exception;
            $details    = $exception->getDetails() == '' ? $exception->getDetails() : ': ' . $exception->getDetails();
            $inner      = $exception->getInnerException();

            if ($inner) {
                $tracer = $inner;
                $messages[] = 'Exception type: ' . get_class($inner);

                if ($this->extraDebug) {
                    $messages[] = 'Exception message: ' . $inner->getMessage();
                    $messages[] = 'Exception location: ' . $inner->getFile() . ':' . $inner->getFile();
                }
            }

            if ($this->extraDebug) {
                Utility::exceptionTraceDump($tracer);
            }

            $code = $exception->getHttpCode() ?: 400;

            if (date('jn') === '14') {
                $code = 418;
            }

            if (is_array($exception->getContext())) {
                $messages = array_merge(['exception' => $messages], $exception->getContext());
            }

            $topMessage = $exception->getTopMessageOverride() ? $exception->getTopMessageOverride()
                : $this->getTopMessage($event->getRequest(), $exception->getJtErrorCode());

            $event->setResponse($this->errorJson($topMessage . $details, $messages, $code));
        }
    }

    protected function getTopMessage(Request $request, $jtErrorCode)
    {
        $route = $request->get('_route') ?: $request->getRequestUri();
        $verb = $request->getMethod();
        $idx = $verb . ' ' . $route;

        $error = 'Could not ' . Utility::get(self::ROUTE_ERRORS[$idx], "execute $verb on $route");

        switch ($jtErrorCode['code']) {
            case self::ERROR_CODE_INVALID_FIELD:
                $detail = preg_replace('/{field}/', Utility::snake2Words($jtErrorCode['field']),
                    ' because the provided {field} is invalid');
                return $error . $detail;
                break;
            default:
                return $error;
                break;
        }
    }
}
