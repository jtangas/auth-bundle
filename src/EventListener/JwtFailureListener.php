<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 10:11 AM
 */

namespace Jtangas\AuthBundle\EventListener;


use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Symfony\Component\HttpFoundation\JsonResponse;

class JwtFailureListener
{
    /**
     * @param AuthenticationFailureEvent $event
     *
     * @return void
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        $data = [
            'success' => false,
            'message' => 'Bad credentials, please verify your username/password.',
        ];

        $data['event'] = $event;

        $response = new JsonResponse($data, 401);

        $event->setResponse($response);
    }

    /**
     * @param JWTInvalidEvent $event
     *
     * @return void
     */
    public function onJwtInvalid(JWTInvalidEvent $event)
    {
        $data = [
            'success' => false,
            'message' => 'Your token is invalid, please login again to get a new one',
        ];

        $response = new JsonResponse($data, 403);

        $event->setResponse($response);
    }

    /**
     * @param JWTNotFoundEvent $event
     *
     * @return void
     */
    public function onJwtNotFound(JWTNotFoundEvent $event)
    {
        $data = [
            'success' => false,
            'message' => 'Missing token',
        ];

        $response = new JsonResponse($data, 403);

        $event->setResponse($response);
    }
}