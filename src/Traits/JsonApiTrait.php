<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 10:13 AM
 */

namespace Jtangas\AuthBundle\Traits;


use Symfony\Component\HttpFoundation\JsonResponse;

trait JsonApiTrait
{
    public function successJson($message = '', $details = [], $httpStatus = 200)
    {
        if (!is_string($message)) {
            $e = "You're calling it wrong: " . __FILE__ . ":" . __LINE__;
            throw new \Exception($e);
        }

        return new JsonResponse(
            [
                'success'   => true,
                'message'   => $message,
                'data'      => $details,
            ],
            $httpStatus
        );
    }

    public function errorJson($message = '', $errors = [], $httpStatus = 500)
    {
        if (!is_string($message)) {
            $e = "You're calling it wrong: " . __FILE__ . ":" . __LINE__;
            throw new \Exception($e);
        }

        $response = [
            'success' => false,
            'error_code' => $httpStatus,
            'message' => $message,
        ];

        if ($errors) {
            $response['errors'] = $errors;
        }

        return new JsonResponse($response, $httpStatus);
    }
}