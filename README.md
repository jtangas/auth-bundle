Installation
=======

Install the package with composer

```bash
composer require jtangas/auth-bundle
```

Add LexikJWT, FOSUserBundle, and this bundle to app/AppKernel.php

```php
$bundles = [
    ...
    new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
    new Jtangas\AuthBundle\JtangasAuthBundle(),
    new FOS\UserBundle\FOSUserBundle(),
];
```

Follow prompts to generate default values for Lexik JWT

Values
======

| parameter | default value | description |
|:----------|:--------------|------------:|
| jwt_private_key_path | '%kernel.project_dir%/var/jwt/private.pem' | The default location of where the package will build the private pem file for JWT. |
| jwt_public_key_path | '%kernel.prject_dir%/var/jwt/public.pem' | The default location of where the package will build the public pem file for JWT. |
| jwt_key_pass_phrase | null | The password the user must provide to secure the private key file |
| jwt_token_ttl | null | deprecated |
| jwt_token_ttl_admin | null | Sets token TTL for admin level users with FOS User Roles |
| jwt_token_ttl_user | null | Sets token TTL for user level users with FOS User Roles |
| user_identity_field | username | Defines what field in the database will be used as the users login name |
| user_class | null | Defines your user class that will be implementing the FOS User class |
| extra_debug | false | deprecated |
